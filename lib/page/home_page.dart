import 'package:flutter/material.dart';
import 'package:scan_qr/page/profile_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Home Page",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          Text("Hello Word"),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext ctx) => ProfilePage()));
            },
            child: Text("Simpan"),
            style: ElevatedButton.styleFrom(backgroundColor: Colors.blue),
          ),
        ],
      ),
      drawer: Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width - 690,
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 17, bottom: 17),
                child: Text(
                  "Gmail",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 25,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 20),
                child: Divider(color: Colors.black, height: 1.5),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: buildRow(
                  iconData: (Icons.all_inbox_rounded),
                  text: "    Semua kotak masuk",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 10),
                child: Divider(color: Colors.black, height: 1.5),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.inbox),
                  text: "    Utama",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.discount_outlined),
                  text: "    Promosi",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.people_alt_outlined),
                  text: "    Sosial",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.info_outline),
                  text: "    Update",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  "Semua Label",
                  style: TextStyle(
                    fontSize: 10,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20, top: 20),
                child: buildRow(
                  iconData: (Icons.star_border),
                  text: "    Berbintang",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.watch_later_outlined),
                  text: "    Ditunda",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.label_important_outline),
                  text: "    Penting",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.send),
                  text: "    Terkirim",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.schedule_send),
                  text: "    Terjadwal",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.send_time_extension),
                  text: "    Kotak Keluar",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.insert_drive_file_outlined),
                  text: "    Draf",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.outgoing_mail),
                  text: "    Semua Email",
                  fontSize: 15,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: buildRow(
                  iconData: (Icons.info_outlined),
                  text: "    Spam",
                  fontSize: 15,
                ),
              ),
            ],
          )),
    );
  }

  Widget buildRow({required IconData iconData, text, fontSize}) {
    return Row(
      children: [
        Icon(iconData),
        Padding(
            padding: const EdgeInsets.only(left: 5.0, top: 3.0),
            child: Text(
              text,
              style: TextStyle(
                fontSize: fontSize,
              ),
            ))
      ],
    );
  }
}
